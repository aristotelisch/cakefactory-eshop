package eu.happybit.ecommerce.domain;

import lombok.Data;

@Data
public class ProductDTO {
    private final String code;
    private final String title;
    private final String description;
    private final String price;
    private final int rating;
    private final String imgUrl;
}
