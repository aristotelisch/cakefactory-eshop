package eu.happybit.ecommerce.controller;

import eu.happybit.ecommerce.domain.ProductDTO;
import eu.happybit.ecommerce.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/")
public class CatalogController {

  private ProductService productService;

  public CatalogController(ProductService productService) {
    this.productService = productService;
  }

  @GetMapping
  public ModelAndView index(Map<String, Object> model) {

    List<ProductDTO> products = productService.getAllProducts();
    model.put("products", products);

    return new ModelAndView("home", model);
  }
}
