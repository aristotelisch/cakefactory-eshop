package eu.happybit.ecommerce.service;

import eu.happybit.ecommerce.domain.ProductDTO;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Override
    public List<ProductDTO> getAllProducts() {
        return Arrays.asList(
                new ProductDTO(
                        "12341",
                        "Butter Cheese Pie",
                        "The classic cheese pie with feta cheese",
                        "2.00",
                        5,
                        "images/cheese-pie.jpg"),
                new ProductDTO(
                        "12342",
                        "Butter Cheese Pie",
                        "The classic cheese pie with feta cheese",
                        "2.00",
                        4,
                        "images/cheese-pie.jpg"),
                new ProductDTO(
                        "12343",
                        "Butter Cheese Pie",
                        "The classic cheese pie with feta cheese",
                        "2.00",
                        5,
                        "images/cheese-pie.jpg"),
                new ProductDTO(
                        "12344",
                        "Butter Cheese Pie",
                        "The classic cheese pie with feta cheese",
                        "2.00",
                        5,
                        "images/cheese-pie.jpg"),
                new ProductDTO(
                        "12345",
                        "Butter Cheese Pie",
                        "The classic cheese pie with feta cheese",
                        "2.00",
                        5,
                        "images/cheese-pie.jpg"),
                new ProductDTO(
                        "12346",
                        "Butter Cheese Pie",
                        "The classic cheese pie with feta cheese",
                        "2.00",
                        5,
                        "images/cheese-pie.jpg"));
    }
}
