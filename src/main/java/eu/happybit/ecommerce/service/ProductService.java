package eu.happybit.ecommerce.service;

import eu.happybit.ecommerce.domain.ProductDTO;

import java.util.List;

public interface ProductService {
    List<ProductDTO> getAllProducts();
}
