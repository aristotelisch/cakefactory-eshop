package eu.happybit.ecommerce.entity;

import lombok.Data;

@Data
public class Product {
    private final String code;
    private final String title;
    private final String description;
    private final String price;
    private final int rating;
    private final String imgUrl;
}
