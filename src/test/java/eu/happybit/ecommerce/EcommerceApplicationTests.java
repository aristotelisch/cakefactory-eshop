package eu.happybit.ecommerce;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import eu.happybit.ecommerce.controller.CatalogController;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class EcommerceApplicationTests {

  @LocalServerPort private String port;

  @Autowired TestRestTemplate restTemplate;

  @Autowired
  CatalogController homeController;

  @Autowired MockMvc mockMvc;

  private WebClient webClient;

  @Before
  public void init() {
  }
  @Test
  void contextLoads() {
    assertThat(homeController).isNotNull();
  }

  @Test
  void homePageLoads() {
    ResponseEntity<String> entity = this.restTemplate.getForEntity(getBASE_URL(), String.class);

    assertEquals(HttpStatus.OK, entity.getStatusCode());
  }

  @Test
  void catalogTitleContainsCompanyName() throws IOException {
    try (WebClient webClient = new WebClient()) {

      final HtmlPage page = webClient.getPage(this.getBASE_URL());

      assertEquals("Cake Factory - E-shop!", page.getTitleText());

      // Get products
      page.getElementById("products").getChildNodes().forEach(node -> {
        node.getByXPath("//div/div[1]/h4/a").get(0);
      });
      assertEquals(6, page.getElementById("products").getChildElementCount());
    }
  }

  @Test
  void companyNameLoads() throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders.get("/"))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("Cake Factory")));
  }

  private String getBASE_URL() {
    return "http://localhost:" + port;
  }
}
